const arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
let div = document.querySelector("#root")
console.log(div)
console.log(renderArray(arr,div))

function renderArray(arr,parent = document.body){
    const createUl  = document.createElement("ul")
     createUl.insertAdjacentHTML("afterbegin",`${arr.map(item =>{
     if(Array.isArray(item)){
        return `<li>${renderArray(item,createUl)}</li>`
     }else{
        return `<li> ${item}</li>`
     }
     }).join("")}`)
     parent.append(createUl)
   }
  function timeOut(time){
     let timer = document.createElement("span")
     timer.innerText = time
     document.body.append(timer)
      const ul = document.querySelector("ul")
      
      let interval = setInterval(() => {
        timer.innerText = --time
        if(time === -1){
           ul.remove()
           timer.remove()
        }
      }, 1000);
      if(timer<0){
        clearInterval(interval)
      }
  }
  timeOut(3)